import { Storage } from '@ionic/storage';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';



@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
tokenUser:any;
   
  constructor(public navCtrl: NavController, 
    private storage: Storage) {

    storage.get('token').then((val) => {
      this.tokenUser=val;
    });
  }

}
