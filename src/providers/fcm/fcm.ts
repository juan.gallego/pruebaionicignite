import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Firebase } from '@ionic-native/firebase';
import { Platform } from 'ionic-angular';
import { AngularFirestore } from 'angularfire2/firestore';
import { Storage } from '@ionic/storage';
import { firebaseConfig2 } from '../../environments/firebase-config2';
import { AngularFireModule } from 'angularfire2';
import firebase from 'firebase';

/*
  Generated class for the FcmProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class FcmProvider {

  hh: any
  db: any
  fire: any

  constructor(
    public firebaseNative: Firebase,
    public afs: AngularFirestore,
    private platform: Platform,
    private storage: Storage
  ) {
    this.hh = firebase.initializeApp(firebaseConfig2, 'Mrquick')
    this.db = this.hh.database()

  }


  // Save the token to firestore
  private saveTokenToFirestore(token) {
    if (!token) return;

    this.db.ref('devices/' + token).set({
      userId: 'hola mundo',
      appId: 'ignite-prueba'
    });

    const devicesRef = this.afs.collection('devices')

    const docData = {
      token,
      userId: 'hola mundo',
      appId: 'ignite-prueba'
    }
    return devicesRef.doc(token).set(docData)
  }

  // Listen to incoming FCM messages
  listenToNotifications() {
    return this.firebaseNative.onNotificationOpen()
  }

  // Get permission from the user
  async getToken() {

    let token;
    let token2;

    if (this.platform.is('android')) {
      token = await this.firebaseNative.getToken()
    }

    if (this.platform.is('ios')) {
      token = await this.firebaseNative.getToken();
      await this.firebaseNative.grantPermission();
    }
    this.storage.set('token', JSON.stringify(token));
    return this.saveTokenToFirestore(token)
  }

}
