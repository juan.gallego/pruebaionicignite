import { firebaseConfig2 } from './../environments/firebase-config2';
import { AngularFireModule } from 'angularfire2';
import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { FcmProvider } from '../providers/fcm/fcm';
import { ToastController } from 'ionic-angular';
import firebase from 'firebase';
import { Subject } from 'rxjs/Subject';
import { tap } from 'rxjs/operators';



import { TabsPage } from '../pages/tabs/tabs';
import { firebaseConfig } from '../environments/firebase-config';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any = TabsPage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, fcm: FcmProvider, toastCtrl: ToastController) {
    platform.ready().then(() => {
     

      // Get a FCM token
      fcm.getToken()



      // Listen to incoming messages
      fcm.listenToNotifications().pipe(
        tap(msg => {
          // show a toast
          const toast = toastCtrl.create({
            message: msg.body,
            duration: 3000
          });
          toast.present();
        })
      )
        .subscribe()
    });
   
    
  }
}
